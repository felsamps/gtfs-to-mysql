#shape_id,shape_pt_lat,shape_pt_lon,shape_pt_sequence,shape_dist_traveled
#p_176623,-29.226653,-51.348068,1,0

CREATE TABLE `shapes` (
  shape_id VARCHAR(50),
  shape_pt_lat DECIMAL(9,6),
  shape_pt_lon DECIMAL(9,6),
  shape_pt_sequence INT(11),
  shape_dist_traveled DECIMAL(20,6),
  KEY `shape_id` (shape_id)
);
LOAD DATA LOCAL INFILE 'transit-data-frp/shapes.txt' INTO TABLE shapes FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' IGNORE 1 LINES;
