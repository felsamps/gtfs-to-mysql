#stop_id,stop_code,platform_code,stop_name,stop_desc,stop_lat,stop_lon,zone_id,stop_url,location_type,parent_station,stop_timezone,position,direction,wheelchair_boarding
#2429586,,,Rua Sao Leopoldo com Rua Montenegro,,-29.22596411,-51.32204711,,,0,,America/Sao_Paulo,,,0


CREATE TABLE `stops` (
  stop_id INT PRIMARY KEY,
  stop_code VARCHAR(10),
  platform_code VARCHAR(10),
  stop_name VARCHAR(255),
  stop_desc VARCHAR(255),
  stop_lat DECIMAL(9,6),
  stop_lon DECIMAL(9,6),
  zone_id INT,
  stop_url VARCHAR(50),
  location_type TINYINT,
  parent_station INT,
  stop_timezone VARCHAR(20),
  position INT,       #not in the specification
  direction_id INT,   #not in the specification
  wheelchair_boarding TINYINT,
  KEY `stop_lat` (stop_lat),
  KEY `stop_lon` (stop_lon)
);


LOAD DATA LOCAL INFILE 'transit-data-frp/stops.txt' INTO TABLE stops FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' IGNORE 1 LINES;
