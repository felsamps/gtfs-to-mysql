#agency_id,route_id,route_short_name,route_long_name,route_desc,route_type,route_url,route_color,route_text_color,route_sort_order,min_headway_minutes,eligibility_restricted
#787,9615,141,Praça x São João,,3,,bfbfbf,000000,1,,0

CREATE TABLE `routes` (
  agency_id INT(11),
  route_id INT(11) PRIMARY KEY,
  route_short_name VARCHAR(50),
  route_long_name VARCHAR(255),
  route_desc VARCHAR(255),
  route_type TINYINT,
  route_url VARCHAR(50),
  route_color VARCHAR(11),
  route_text_color VARCHAR(11),
  route_sort_order INT(11),
  min_headway_minutes INT(11),
  eligibility_restricted INT(2),
  KEY `route_type` (route_type),

  FOREIGN KEY (agency_id) REFERENCES agency(agency_id)
);


LOAD DATA LOCAL INFILE 'transit-data-frp/routes.txt' INTO TABLE routes FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' IGNORE 1 LINES;
