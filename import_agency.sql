#agency_id,agency_url,agency_lang,agency_name,agency_phone,agency_timezone,agency_fare_url
#787,https://docs.google.com/spreadsheets/d/1S4Wvw-38KpClHwRC90uQWk0h0nBKIw0rpiTqEUAJKuI/edit?ts=5a21a3fc#gid=0,pt,Cidade de Farroupilha Transporte Colectivo,(54) 3542.2977,America/Sao_Paulo,

CREATE TABLE `agency` (
  agency_id int(11) PRIMARY KEY,
  agency_url VARCHAR(255),
  agency_lang VARCHAR(10),
  agency_name VARCHAR(255),
  agency_phone VARCHAR(50),
  agency_timezone VARCHAR(50),
  agency_fare_url VARCHAR(255)
);

LOAD DATA LOCAL INFILE 'transit-data-frp/agency.txt' INTO TABLE agency FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' IGNORE 1 LINES;
