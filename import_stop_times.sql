#trip_id,arrival_time,departure_time,stop_id,stop_sequence,stop_headsign,pickup_type,drop_off_type,shape_dist_traveled,timepoint,start_service_area_id,end_service_area_id,start_service_area_radius,end_service_area_radius,continuous_pickup,continuous_drop_off,pickup_area_id,drop_off_area_id,pickup_service_area_radius,drop_off_service_area_radius
#t_201970_b_6309_tn_0,12:25:00,12:25:00,2429823,1,,0,0,0,0,,,,,1,1,,,,


CREATE TABLE `stop_times` (
  trip_id VARCHAR(20) NOT NULL,
  arrival_time VARCHAR(8),    #time
  departure_time VARCHAR(8),  #time
  stop_id INT NOT NULL,
  stop_sequence INT NOT NULL,
  stop_headsign VARCHAR(20),
  pickup_type TINYINT ,
  drop_off_type TINYINT,
  shape_dist_traveled DECIMAL(15,10),
  timepoint TINYINT,
  start_service_area_id VARCHAR(11),    #these fields onwards are not in the specification
  end_service_area_id VARCHAR(11),
  start_service_area_radius VARCHAR(11),
  end_service_area_radius VARCHAR(11),
  continuous_pickup VARCHAR(11),
  continuous_drop_off VARCHAR(11),
  pickup_area_id VARCHAR(11),
  drop_off_area_id VARCHAR(11),
  pickup_service_area_radius VARCHAR(11),
  drop_off_service_area_radius VARCHAR(11),

  KEY `trip_id` (trip_id),
  KEY `stop_id` (stop_id),
  KEY `stop_sequence` (stop_sequence),

  FOREIGN KEY (trip_id) REFERENCES trips(trip_id),
  FOREIGN KEY (stop_id) REFERENCES stops(stop_id)
);


LOAD DATA LOCAL INFILE 'transit-data-frp/stop_times.txt' INTO TABLE stop_times FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' IGNORE 1 LINES;
