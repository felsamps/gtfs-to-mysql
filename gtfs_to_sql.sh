MYSQL_USER="root"
MYSQL_SERVER_IP="localhost"
TMP_SCRIPT=".tmp.script.sql"

#echo "" > $TMP_SCRIPT
cat drop_tables.sql > $TMP_SCRIPT
cat import_agency.sql >> $TMP_SCRIPT
cat import_shapes.sql >> $TMP_SCRIPT
cat import_routes.sql >> $TMP_SCRIPT
cat import_stops.sql >> $TMP_SCRIPT
cat import_trips.sql >> $TMP_SCRIPT
cat import_stop_times.sql >> $TMP_SCRIPT

cat $TMP_SCRIPT |sudo  mysql -u $MYSQL_USER -p -h $MYSQL_SERVER_IP
#rm $TMP_SCRIPT
