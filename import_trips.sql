#route_id,service_id,trip_id,trip_short_name,trip_headsign,direction_id,block_id,shape_id,bikes_allowed,wheelchair_accessible,trip_type,drt_max_travel_time,drt_avg_travel_time,drt_advance_book_min,drt_pickup_message,drt_drop_off_message,continuous_pickup_message,continuous_drop_off_message
#9509,c_5988_b_6309_d_31,t_201970_b_6309_tn_0,Praçã x Vicentina,,0,12506,p_176658,,,,,,,,,,

CREATE TABLE `trips` (
  route_id INT,
  service_id VARCHAR(30),
  trip_id VARCHAR(20) PRIMARY KEY,
  trip_short_name VARCHAR(50),
  trip_headsign VARCHAR(100),
  direction_id TINYINT,
  block_id INT,
  shape_id VARCHAR(20),
  bikes_allowed TINYINT,
  wheelchair_accessible TINYINT,
  trip_type INT(11),
  drt_max_travel_time INT(11),
  drt_avg_travel_time INT(11),
  drt_advance_book_min INT(11),
  drt_pickup_message INT(11),
  drt_drop_off_message INT(11),
  continuous_pickup_message VARCHAR(50),
  continuous_drop_off_message VARCHAR(50),

  KEY `route_id` (route_id),
  KEY `service_id` (service_id),
  KEY `direction_id` (direction_id),
  KEY `shape_id` (shape_id),

  FOREIGN KEY (route_id) REFERENCES routes(route_id),
  FOREIGN KEY (shape_id) REFERENCES shapes(shape_id)
  #FOREIGN KEY service_id REFERENCES calendar(service_id),
  #FOREIGN KEY service_id REFERENCES calendar_dates(service_id),

);


LOAD DATA LOCAL INFILE 'transit-data-frp/trips.txt' INTO TABLE trips FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' IGNORE 1 LINES;
